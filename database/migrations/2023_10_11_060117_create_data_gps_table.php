<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_gps', function (Blueprint $table) {
            $table->id();
            $table->string('nama_costumer');
            $table->date('tanggal_pemasangan');
            $table->string('email_byu')->nullable();
            $table->string('password_byu')->nullable();
            $table->string('username');
            $table->string('password')->nullable();
            $table->bigInteger('imei');
            $table->bigInteger('nomer_hp');
            $table->date('masa_aktif');
            $table->string('server');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_gps');
    }
};
