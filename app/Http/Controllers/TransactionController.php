<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Transaction;

class TransactionController extends Controller
{
    public function index()
    {
        $transactions = Transaction::with('items.product')->get();

        return view('pages.admin.transaksi.datatransaksi', compact('transactions'));
    }
}
