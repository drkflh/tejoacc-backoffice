<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\dataGPS;

class DataGpsController extends Controller
{

    public function index()
    {
        $currentDate = now();

        $gps = dataGPS::select('id', 'nama_costumer', 'username', 'masa_aktif', 'server')
                        ->orderByRaw("CASE WHEN masa_aktif >= '$currentDate' THEN 0 ELSE 1 END") 
                        ->orderBy('masa_aktif', 'asc') 
                        ->get();
    
        return view('pages.admin.gpstracker.listgps', compact('gps'));
    }


    public function create()
    {
        return view('pages.admin.gpstracker.tambahdatagps');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'Nama_Costumer' => 'required',
            'Tanggal_Pemasangan' => 'required',
            'Email_BYU' => 'nullable',
            'Password_BYU' => 'nullable',
            'Username' => 'required',
            'Password' => 'nullable',
            'Imei' => 'required',
            'Nomer_HP' => 'required',
            'Masa_Aktif' => 'required',
            'Server' => 'required',
        ]);

        $formData = new dataGPS();
        $formData->Nama_Costumer = $request->input('Nama_Costumer');
        $formData->Tanggal_Pemasangan = $request->input('Tanggal_Pemasangan');
        $formData->Email_BYU = $request->input('Email_BYU');
        $formData->Password_BYU = $request->input('Password_BYU');
        $formData->Username = $request->input('Username');
        $formData->Password = $request->input('Password');
        $formData->Imei = $request->input('Imei');
        $formData->Nomer_HP = $request->input('Nomer_HP');
        $formData->Masa_Aktif = $request->input('Masa_Aktif');
        $formData->Server = $request->input('Server');
        $formData->save();

        return redirect()->route('listgps')->with('success', 'Data berhasil disimpan.');
    }

    public function show($id)
    {
        $gps = dataGPS::find($id);

        return view('pages.admin.gpstracker.detailgps', ['gps' => $gps]);
    }

    public function edit($id)
    {
        $gps = dataGPS::find($id);

        if (!$gps) {
            return redirect()->route('listgps')->with('error', 'Postingan tidak ditemukan.');
        }

        return view('pages.admin.gpstracker.editdatagps', compact('gps'));
    }

    public function update(Request $request, $id)
    {
        $gps = dataGPS::find($id);

        $gps->nama_costumer = $request->input('nama_costumer');
        $gps->tanggal_pemasangan = $request->input('tanggal_pemasangan');
        $gps->email_byu = $request->input('email_byu');
        $gps->password_byu = $request->input('password_byu');
        $gps->username = $request->input('username');
        $gps->password = $request->input('password');
        $gps->imei = $request->input('imei');
        $gps->nomer_hp = $request->input('nomer_hp');
        $gps->masa_aktif = $request->input('masa_aktif');
        $gps->server = $request->input('server');

        $gps->save();

        return redirect()->route('listgps')->with('success', 'Postingan berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $gps = dataGPS::find($id);

        if (!$gps) {
            return redirect()->route('listgps')->with('error', 'Postingan tidak ditemukan.');
        }

        $gps->delete();

        return redirect()->route('listgps')->with('success', 'Postingan berhasil dihapus.');
    }

}