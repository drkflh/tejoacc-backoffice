<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class MemberController extends Controller
{
    public function index()
    {
        $members = User::where('role', 'user')->get();

        return view('pages.admin.member.listmember', compact('members'));
    }
}