<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('pages.admin.product.listproduct', ['products' => $products]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'edit_nama_barang' => 'required|string',
            'edit_stok_barang' => 'required|numeric',
            'edit_harga_barang' => 'required|numeric',
            'edit_kategori_barang' => 'required|string',
            'edit_keterangan_barang' => 'required|string',
            'edit_gambar_barang' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $product = Product::findOrFail($id);

        $product->nama_barang = $request->input('edit_nama_barang');
        $product->stok_barang = $request->input('edit_stok_barang');
        $product->harga_barang = $request->input('edit_harga_barang');
        $product->kategori_barang = $request->input('edit_kategori_barang');
        $product->keterangan_barang = $request->input('edit_keterangan_barang');

        if ($request->hasFile('edit_gambar_barang')) {
            $image = $request->file('edit_gambar_barang');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('gambar_produk', $imageName, 'public');
            $product->gambar_barang = $imageName;
        }

        $product->save();

        return redirect()->route('produk')->with('success', 'Product updated successfully');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_barang' => 'required|string|max:255',
            'stok_barang' => 'required|integer|min:0',
            'harga_barang' => 'required|numeric|min:0',
            'kategori_barang' => 'required|string|max:255',
            'gambar_barang' => 'required|image|mimes:jpeg,png',
            'keterangan_barang' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $input = $request->all();

        if ($request->hasFile('gambar_barang')) {
            $image = $request->file('gambar_barang');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/gambar_produk', $imageName);
            $input['gambar_barang'] = $imageName;
        }

        Product::create($input);

        return redirect()->route('produk')
            ->with('success', 'Produk berhasil ditambahkan');
    }

    public function destroy($id)
    {
        $products = Product::find($id);

        if (!$products) {
            return redirect()->route('produk')
                ->with('error', 'produk not found');
        }

        $products->delete();

        return redirect()->route('produk')
            ->with('success', 'produk deleted successfully');
    }
}