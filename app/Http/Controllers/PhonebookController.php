<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Phonebook;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class PhonebookController extends Controller
{
    public function index()
    {
        $phonebook = Phonebook::select('id', 'nama_kontak', 'nomer_kontak')
                        ->get();

        return view('pages.admin.whatsappgateway.phonebook', compact('phonebook'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'newNama' => 'required|string|max:255',
            'newNomer' => 'required|numeric|unique:phonebooks,nomer_kontak',
        ]);

        if ($validator->fails()) {
            return redirect()->route('phonebook.index')
                ->withErrors($validator)
                ->withInput();
        }

        Phonebook::create([
            'nama_kontak' => $request->input('newNama'),
            'nomer_kontak' => $request->input('newNomer'),
        ]);

        return redirect()->route('phonebook')->with('success', 'Data berhasil ditambahkan.');
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'editedNomer' => [
                'required',
                'numeric',
                Rule::unique('phonebooks', 'nomer_kontak')->ignore($id),
            ],
        ]);

        if ($validator->fails()) {
            return redirect()->route('phonebook.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }

        $phonebook = Phonebook::findOrFail($id);
        $phonebook->nomer_kontak = $request->input('editedNomer');
        $phonebook->save();

        return redirect()->route('phonebook')->with('success', 'Data berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $phonebook = Phonebook::find($id);

        if (!$phonebook) {
            return redirect()->route('phonebook')->with('error', 'Postingan tidak ditemukan.');
        }

        $phonebook->delete();

        return redirect()->route('phonebook')->with('success', 'Postingan berhasil dihapus.');
    }
}
