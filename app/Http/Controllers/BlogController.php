<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Blog;


class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();
        return view('pages.admin.blog.listblog', compact('blogs'));
    }

    public function indexuser()
    {
        $blogs = Blog::all();
        return view('pages.user.blog', compact('blogs'));
    }

    public function showuser($id)
    {
        $blog = Blog::findOrFail($id);

        return view('pages.user.blogdetail', compact('blog'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul_blog' => 'required',
            'isi_blog' => 'required',
            'gambar_blog' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $input = $request->all();

        if ($request->hasFile('gambar_blog')) {
            $image = $request->file('gambar_blog');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('public/gambar_blog', $imageName);
            $input['gambar_blog'] = $imageName;
        }

        Blog::create($input);

        return redirect()->route('blog')
                        ->with('success', 'Blog berhasil ditambahkan!');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'edit_judul_blog' => 'required|string',
            'edit_isi_blog' => 'required|string',
            'edit_gambar_blog' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $blog = Blog::findOrFail($id);

        $blog->judul_blog = $request->input('edit_judul_blog');
        $blog->isi_blog = $request->input('edit_isi_blog');

        if ($request->hasFile('edit_gambar_blog')) {
            $image = $request->file('edit_gambar_blog');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->storeAs('gambar_blog', $imageName, 'public');
            $blog->gambar_blog = $imageName;
        }

        $blog->save();

        return redirect()->route('blog')->with('success', 'Blog updated successfully');
    }

    public function destroy($id)
    {
        $blogs = Blog::find($id);

        if (!$blogs) {
            return redirect()->route('blog')
            ->with('error', 'blog not found');
        }

        $blogs->delete();

        return redirect()->route('blog')
            ->with('success', 'blog deleted successfully');
    }
}