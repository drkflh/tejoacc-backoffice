<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MessageHistory;

class MessageHistoryController extends Controller
{
    public function index()
    {
        $messageHistories = MessageHistory::latest()->get();
        return view('pages.admin.whatsappgateway.historymessage', compact('messageHistories'));
    }
}
