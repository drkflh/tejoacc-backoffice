<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Phonebook;
use App\Models\TemplateMessage;
use Illuminate\Support\Facades\Http;
use App\Models\ApiConfiguration;
use Carbon\Carbon;
use App\Models\MessageHistory;
use Illuminate\Support\Facades\Log;



class SendMessageController extends Controller
{
    public function index()
    {
        $phonebooks = Phonebook::all();
        $templates = TemplateMessage::all();
        return view('pages.admin.whatsappgateway.send-message', compact('phonebooks', 'templates'));
    }

    public function sendWhatsAppMessage(Request $request)
    {
        $apiConfiguration = ApiConfiguration::first();

        if (!$apiConfiguration) {
            return redirect()->back()->with('error', 'Konfigurasi API tidak ditemukan.');
        }

        $apiUrl = rtrim($apiConfiguration->api_url, '/') . '/send';
        $authorizationToken = $apiConfiguration->authorization_token;

        $headers = [
            'Authorization' => $authorizationToken,
            'Content-Type' => 'application/json',
        ];

        $selectedPhonebookIds = $request->input('selected_phonebooks');

        if (!is_array($selectedPhonebookIds) || empty($selectedPhonebookIds)) {
            return redirect()->back()->with('error', 'Tidak ada nomor telepon terpilih.');
        }

        $phonebookNumbers = Phonebook::whereIn('id', $selectedPhonebookIds)->pluck('nomer_kontak')->toArray();

        foreach ($phonebookNumbers as $phoneNumber) {
            $messageType = $request->input('message_type');

            if ($messageType == 'text') {
                $message = $request->input('text_message');
            } elseif ($messageType == 'template') {
                $templateMessage = $request->input('template_message');

                $phonebook = Phonebook::find($selectedPhonebookIds[0]);
                $contactName = $phonebook ? $phonebook->nama_kontak : 'Nama Tidak Ditemukan';
                $message = str_replace('{nama}', $contactName, $templateMessage);
            }

            $wibSchedule = $request->input('schedule');
            $scheduleTimestamp = $wibSchedule ? $this->convertToUtc0Timestamp($wibSchedule) : time();

            $data = [
                'target' => $phoneNumber,
                'message' => $message,
                'schedule' => $scheduleTimestamp,
            ];

            try {
                $response = Http::withHeaders($headers)->post($apiUrl, $data);

                $statusCode = $response->status();
                $responseData = $response->json();

                $this->saveMessageHistory($phoneNumber, $message, $scheduleTimestamp);
            } catch (\Exception $e) {
                $errorMessage = $e->getMessage();
            }
        }

        return redirect()->back()->with('status', 'Pesan WhatsApp berhasil dikirim!');
    }

    private function convertToUtc0Timestamp($wibSchedule)
    {
        $scheduleDateTime = Carbon::createFromFormat('Y-m-d\TH:i', $wibSchedule, 'Asia/Jakarta');
        $scheduleDateTimeUtc0 = $scheduleDateTime->setTimezone('UTC');
        return $scheduleDateTimeUtc0->timestamp;
    }

    private function saveMessageHistory($phoneNumber, $message)
    {
        try {
            $data = [
                'target' => $phoneNumber,
                'message' => $message,
                'schedule_timestamp' => time(),
            ];

            MessageHistory::create($data);
        } catch (\Exception $e) {
            \Log::error('Error saving message history: ' . $e->getMessage());
            dd($e->getMessage());
        }
    }


}
