<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Blog;

class DashboardUserController extends Controller
{
    public function index()
    {
        $latestProducts = Product::latest()->limit(10)->get();
        $latestBlogs = Blog::latest()->limit(10)->get();

        return view('pages.user.index', compact('latestProducts', 'latestBlogs'));
    }
}