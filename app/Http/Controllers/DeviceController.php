<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\ApiConfiguration;

class DeviceController extends Controller
{
    public function index()
    {
        $apiConfiguration = ApiConfiguration::first();

        if (!$apiConfiguration) {
            return redirect()->back()->with('error', 'Konfigurasi API tidak ditemukan.');
        }

        $apiUrl = rtrim($apiConfiguration->api_url, '/') . '/device';
        $authorizationToken = $apiConfiguration->authorization_token;

        $response = Http::withHeaders([
            'Authorization' => $authorizationToken,
        ])->post($apiUrl);

        $deviceData = $response->json();

        return view('pages.admin.pengaturan.device', compact('deviceData'));
    }

    public function connect()
    {
        $apiConfiguration = ApiConfiguration::first();

        if (!$apiConfiguration) {
            return redirect()->back()->with('error', 'Konfigurasi API tidak ditemukan.');
        }

        $apiUrl = rtrim($apiConfiguration->api_url, '/') . '/qr';
        $authorizationToken = $apiConfiguration->authorization_token;

        $response = Http::withHeaders(['Authorization' => $authorizationToken])
            ->post($apiUrl);

        if ($response->successful()) {
            $qrCode = $response->json('url');
            return view('pages.admin.pengaturan.deviceqr', ['qrCode' => $qrCode]);
        } else {
            return response()->json(['error' => 'Failed to connect. Please try again.'], 500);
        }
    }
}