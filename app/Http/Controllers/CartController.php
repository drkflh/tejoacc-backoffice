<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CartController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $cartItems = $user->cartItems;

        return view('pages.user.cart', compact('cartItems'));
    }

    public function destroy($id)
    {
        $user = Auth::user();
        $cartItem = Cart::findOrFail($id);

        if ($cartItem->user_id != $user->id) {
            abort(403);
        }

        $cartItem->delete();

        return redirect()->route('keranjang')->with('success', 'Item removed from cart successfully.');
    }

    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required|exists:products,id',
        ]);

        Cart::create([
            'user_id' => auth()->id(),
            'product_id' => $request->product_id,
        ]);

        return redirect()->back()->with('success', 'Item berhasil ditambahkan ke keranjang.');
    }

}