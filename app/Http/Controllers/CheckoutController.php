<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Models\TransactionItem;
use App\Models\Product;

class CheckoutController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $cartItems = $user->cartItems;

        return view('pages.user.checkout', compact('cartItems', 'user'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string',
            'email' => 'required|email',
            'nomer_hp' => 'required|string',
            'lokasi_pemasangan' => 'required|string',
            'cartItems' => 'required|array',
            'cartItems.*.product_id' => 'required|exists:products,id',
        ]);

        DB::beginTransaction();

        try {
            $transaction = Transaction::create([
                'nama' => $request->nama,
                'email' => $request->email,
                'nomer_hp' => $request->nomer_hp,
                'lokasi_pemasangan' => $request->lokasi_pemasangan,
            ]);

            $totalHarga = 0;

            $message = "Halo " . $request->nama . ",\n\n";
            $message .= "Terima kasih telah melakukan pembelian di TEJO ACC! Berikut adalah konfirmasi pesanan Anda:\n\n";
            $message .= "Nama: " . $request->nama . "\n";
            $message .= "Email: " . $request->email . "\n";
            $message .= "Nomor HP: " . $request->nomer_hp . "\n";
            $message .= "Lokasi Pemasangan: " . $request->lokasi_pemasangan . "\n\n";

            $message .= "Pesanan Anda:\n";
            foreach ($request->cartItems as $index => $cartItem) {
                $product = Product::findOrFail($cartItem['product_id']);

                $product->stok_barang -= 1;
                $product->save();

                TransactionItem::create([
                    'transaction_id' => $transaction->id,
                    'product_id' => $product->id,
                    'product_name' => $product->nama_barang,
                    'price' => $product->harga_barang,
                ]);

                $totalHarga += $product->harga_barang;

                $message .= ($index + 1) . ". " . $product->nama_barang . " - Rp" . number_format($product->harga_barang, 0, ',', '.') . "\n";
            }

            $message .= "\nTotal Harga: Rp" . number_format($totalHarga, 0, ',', '.') . "\n\n";

            $message .= "Pesan ini merupakan konfirmasi bahwa pesanan Anda telah berhasil diproses dan akan segera kami proses lebih lanjut. Kami akan mengonfirmasi ulang pesanan Anda segera setelah kami mempersiapkan barang.\n\n";
            $message .= "Terima kasih atas kepercayaan Anda berbelanja di TEJO ACC!";

            $user = Auth::user();
            $user->cartItems()->delete();

            DB::commit();

            $phoneNumber = $request->nomer_hp;
            $apiUrl = 'https://api.fonnte.com/send';
            $authorizationToken = 'pYeJmYTRyhSAJiq!1oa!';

            $headers = [
                'Authorization' => $authorizationToken,
                'Content-Type' => 'application/json',
            ];

            $data = [
                'target' => $phoneNumber,
                'message' => $message,
            ];

            $client = new \GuzzleHttp\Client();
            $response = $client->post($apiUrl, [
                'headers' => $headers,
                'json' => $data,
            ]);

            return redirect()->route('dashboard')->with('success', 'Checkout berhasil! Pesan WhatsApp telah dikirim.');
        } catch (\Exception $e) {
            DB::rollback();

            return back()->with('error', 'Checkout gagal. Silakan coba lagi.');
        }
    }
}
