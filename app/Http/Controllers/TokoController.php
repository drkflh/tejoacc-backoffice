<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class TokoController extends Controller
{
    public function index(Request $request)
    {
        $productsQuery = Product::query();

        $searchTerm = $request->input('search');
        if ($searchTerm) {
            $productsQuery->where('nama_barang', 'like', '%' . $searchTerm . '%');
        }

        $products = $productsQuery->paginate(9);

        return view('pages.user.shop', compact('products', 'searchTerm'));
    }

    public function show($id)
    {
        $product = Product::find($id);

        if (!$product) {
            abort(404);
        }

        $relatedProducts = Product::inRandomOrder()
            ->where('kategori_barang', '=', $product->kategori_barang)
            ->where('id', '<>', $product->id)
            ->limit(10)
            ->get();

        return view('pages.user.shopdetail', compact('product', 'relatedProducts'));
    }

}
