<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TemplateMessage;

class TemplateMessageController extends Controller
{
    public function index()
    {
        $templates = TemplateMessage::all();
        return view('pages.admin.whatsappgateway.template', compact('templates'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_template' => 'required',
            'pesan_template' => 'required',
        ]);

        TemplateMessage::create($request->all());

        return redirect()->route('template')->with('success', 'Template created successfully');
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_template' => 'required',
            'pesan_template' => 'required',
        ]);

        $template = TemplateMessage::find($id);

        if (!$template) {
            return redirect()->route('template')
                ->with('error', 'Template not found');
        }

        $template->update($request->all());

        return redirect()->route('template')
            ->with('success', 'Template updated successfully');
    }


    public function destroy($id)
    {
        $template = TemplateMessage::find($id);

        if (!$template) {
            return redirect()->route('template')
                ->with('error', 'Template not found');
        }

        $template->delete();

        return redirect()->route('template')
            ->with('success', 'Template deleted successfully');
    }

}
