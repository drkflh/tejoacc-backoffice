<?php
namespace App\Http\Controllers;

use App\Models\dataGPS;
use App\Models\Product;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $currentDate = Carbon::now();

        $dataGPS = dataGPS::where('masa_aktif', '>=', now())
        ->orderBy('masa_aktif', 'asc')
        ->get();

        $data = dataGPS::select('tanggal_pemasangan')->get();

        $labels = [];
        $dataCount = [];

        foreach ($data as $row) {
            $date = Carbon::parse($row->tanggal_pemasangan)->format('Y-m-d');

            if (!in_array($date, $labels)) {
                $labels[] = $date;
                $dataCount[$date] = 1;
            } else {
                $dataCount[$date]++;
            }
        }

        $members = User::where('role', 'user')->count();
        $jumlahDataGPS = dataGPS::count();
        $jumlahDataProduct = Product::count();

        return view('home', compact('jumlahDataGPS', 'labels', 'dataCount', 'dataGPS', 'currentDate', 'jumlahDataProduct', 'members'));
    }
}