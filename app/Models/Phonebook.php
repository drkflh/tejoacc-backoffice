<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phonebook extends Model
{
    use HasFactory;
    protected $table = 'phonebooks';
    protected $fillable = ['id','nama_kontak',
                            'nomer_kontak'];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}