<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dataGPS extends Model
{
    use HasFactory;
    protected $table = 'data_gps';
    protected $fillable = ['id','nama_costumer',
                            'tanggal_pemasangan', 'email_byu', 'password_byu',
                            'username', 'password', 'imei', 'nomer_hp', 'masa_aktif',
                            'server'];
}
