<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'id',
        'nama_barang',
        'stok_barang',
        'harga_barang',
        'kategori_barang',
        'gambar_barang',
        'keterangan_barang',
    ];
}