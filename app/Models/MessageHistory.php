<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageHistory extends Model
{
    use HasFactory;
    protected $table = 'message_histories';
    protected $fillable = ['target', 'message', 'schedule_timestamp'];

}
