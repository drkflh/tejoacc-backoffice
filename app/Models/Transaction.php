<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\TransactionItem;

class Transaction extends Model
{
    use HasFactory;
    protected $table = 'transactions';
    protected $fillable = ['nama', 'email', 'nomer_hp', 'lokasi_pemasangan'];

    public function items()
    {
        return $this->hasMany(TransactionItem::class);
    }
}
