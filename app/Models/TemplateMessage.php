<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateMessage extends Model
{
    use HasFactory;
    protected $table = 'templatemessage';
    protected $fillable = [
        'id',
        'nama_template',
        'pesan_template',
    ];
}
