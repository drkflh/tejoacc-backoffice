<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Telegram;
use App\Models\dataGPS;

class Kernel extends ConsoleKernel
{


    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            Telegram::sendMessage([
                'chat_id' =>  1223650737,
                'text' => 'Apakah Masuk Bangg '
            ]);
        })->everyminute();

        $schedule->call(function () {
            $data = dataGPS::whereDate('masa_aktif', '<=', now()->addDays(3))
                ->get();

            foreach ($data as $item) {
                $chatId = $item->telegram_chat_id;
                $namaCostumer = $item->nama_costumer;
                $nomerHP = $item->nomer_hp;
                $masaAktif = $item->masa_aktif;
                $imei = $item->imei;
                $whatsappNumber = '6282221860948';
                $whatsappLink = "[Hubungi Kami](https://wa.me/$whatsappNumber)";
                $whatsappLinkButton = "https://wa.me/6282221860948";
                $message = "Hallooo $namaCostumer, \nMasa aktif Kouta GPS Anda mendekati kadaluarsa yaitu : \n\nTANGGAL : $masaAktif \nNOMER :  $nomerHP \nIMEI : $imei \n\nSegera perpanjang kouta Anda atau bisa $whatsappLink.";
                $keyboard = [
                    "inline_keyboard" => [
                        [
                            [
                                "text" => "Butuh Bantuan?",
                                "url" => $whatsappLinkButton
                            ]
                        ]
                    ]
                ];

                $markup = json_encode($keyboard);
                Telegram::sendMessage([
                    'chat_id' => $chatId,
                    'text' => $message,
                    'parse_mode' => 'markdown',
                    'disable_web_page_preview' => true,
                    'reply_markup' => $markup
                ]);

            }
        })->everyminute();
    }


    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}