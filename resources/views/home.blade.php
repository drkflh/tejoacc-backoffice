@extends('layouts.app')

@section('title', "Dashboard")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-shopping-bag"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Jumlah GPS</h4>
                        </div>
                        <div class="card-body">
                            {{ $jumlahDataGPS }} Device
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-cube"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Jumlah Produk</h4>
                        </div>
                        <div class="card-body">
                            {{ $jumlahDataProduct }} Produk
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="far fa-user"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Jumlah Member</h4>
                        </div>
                        <div class="card-body">
                            {{ $members }} Member
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Pemasangan GPS</h4>
                    </div>
                    <div class="card-body">
                        <canvas id="grafik" height="160"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card gradient-bottom">
                    <div class="card-header">
                        <h4>Daftar Yang Mendekati Masa Aktif</h4>
                    </div>
                    <div class="card-body" id="top-5-scroll">
                        <ul class="list-unstyled list-unstyled-border">
                            @foreach ($dataGPS as $item)
                            <li class="media">
                                <img class="mr-3 rounded" width="55"
                                    src="{{asset('assets/img/products/product-3-50.png')}}" alt="product">
                                <div class="media-body">
                                    <div class="float-right">
                                        <div class="font-weight-600 text-muted text-small">
                                            {{ now()->diffInDays($item->masa_aktif) }} Hari Lagi</div>
                                    </div>
                                    <div class="media-title">{{ $item->nama_costumer }}</div>
                                    <div class="mt-1">
                                        <div class="budget-price">
                                            <div class="budget-price-square bg-primary" data-width="">
                                            </div>
                                            <div class="budget-price-label"> 0{{ $item->nomer_hp }}</div>
                                        </div>
                                        <div class="budget-price">
                                            <div class="budget-price-square bg-danger"
                                                data-width="{{ $item->budget_price_percentage }}">
                                            </div>
                                            <div class="budget-price-label"> {{ $item->imei }}</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="card-footer pt-3 d-flex justify-content-center">
                        <div class="budget-price justify-content-center">
                            <div class="budget-price-square bg-primary" data-width="20"></div>
                            <div class="budget-price-label">Nomor GPS</div>
                        </div>
                        <div class="budget-price justify-content-center">
                            <div class="budget-price-square bg-danger" data-width="20"></div>
                            <div class="budget-price-label">IMEI</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
var ctx = document.getElementById('grafik').getContext('2d');
var labels = @json($labels);
var data = @json($dataCount);

var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: labels,
        datasets: [{
            label: 'Jumlah Pemasangan',
            data: data,
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 2,
            fill: false,
            tension: 0.2
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: false,
                suggestedMin: 0,
                suggestedMax: 10,
                stepSize: 1,
            }
        }
    }
});
</script>

@endsection