@extends('layouts.main')

@section('title', 'Beranda')

@section('content')
<section class="slider_section slider_two mb-50">
    <div class="slider_area owl-carousel">
        <div class="single_slider d-flex align-items-center"
            data-bgimg="{{asset('assetsuser/img/slider/banner1.png')}}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="slider_content">
                            <h1>Selamat Datang Di</h1>
                            <h1>TEJO ACC</h1>
                            <a class="button" href="/Toko">Buka Toko</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="single_slider d-flex align-items-center"
            data-bgimg="{{asset('assetsuser/img/slider/banner2.png')}}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="slider_content">
                            <h1>Car Audio &</h1>
                            <h1> GPS Tracking</h1>
                            <a class="button" href="/Toko">Buka Toko</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="single_slider d-flex align-items-center"
            data-bgimg="{{asset('assetsuser/img/slider/banner3.png')}}">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="slider_content">
                            <h1>Lighting &</h1>
                            <h1> Accecories</h1>
                            <a class="button" href="/Toko">Buka Toko</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<section class="featured_categories featured_c_four  mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span> <strong>Kategori</strong>Unggulan</span></h2>
                </div>
                <div class="product_carousel featured_four  product_column4 owl-carousel">
                    <div class="single_featured">
                        <div class="featured_thumb">
                            <a href="#"><img src="{{asset('assetsuser/img/featured/gps.jpg')}}" alt=""></a>
                        </div>
                        <div class="featured_content">
                            <h3 class="product_name"><a href="#">Keamanan Mobil</a></h3>
                            <div class="sub_featured">
                                <ul>
                                    <li><a href="#">GPS Tracker</a></li>
                                    <li><a href="#">Alarm</a></li>
                                    <li><a href="#">Central Lock</a></li>
                                </ul>
                            </div>
                            <a class="view_more" href="/toko">Buka Toko</a>
                        </div>
                    </div>
                    <div class="single_featured">
                        <div class="featured_thumb">
                            <a href="#"><img src="{{asset('assetsuser/img/featured/headunit.jpg')}}"
                                    style="width:300px; height:225px;" alt=""></a>
                        </div>
                        <div class="featured_content">
                            <h3 class="product_name"><a href="#">Audio Mobil</a></h3>
                            <div class="sub_featured">
                                <ul>
                                    <li><a href="#">Head Unit</a></li>
                                    <li><a href="#">Amplifier</a></li>
                                    <li><a href="#">Subwoffer</a></li>
                                </ul>
                            </div>
                            <a class="view_more" href="/toko">Buka Toko</a>
                        </div>
                    </div>
                    <div class="single_featured">
                        <div class="featured_thumb">
                            <a href="#"><img src="{{asset('assetsuser/img/featured/lampuhid.webp')}}" alt=""></a>
                        </div>
                        <div class="featured_content">
                            <h3 class="product_name"><a href="#">Pencahayaan</a></h3>
                            <div class="sub_featured">
                                <ul>
                                    <li><a href="#">LAMPU HID</a></li>
                                    <li><a href="#">Lampu Alis Running</a></li>
                                    <li><a href="#">Lampu Plafon</a></li>
                                </ul>
                            </div>
                            <a class="view_more" href="/toko">Buka Toko</a>
                        </div>
                    </div>
                    <div class="single_featured">
                        <div class="featured_thumb">
                            <a href="#"><img src="{{asset('assetsuser/img/featured/talangair.jpg')}}"
                                    style="width:300px; height:230px;" alt=""></a>
                        </div>
                        <div class="featured_content">
                            <h3 class="product_name"><a href="#">Accecories</a></h3>
                            <div class="sub_featured">
                                <ul>
                                    <li><a href="#">Talang Air</a></li>
                                    <li><a href="#">Bumper ARB</a></li>
                                    <li><a href="#">Pematik Api</a></li>
                                </ul>
                            </div>
                            <a class="view_more" href="/toko">Buka Toko</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="banner_area banner_static mb-50 d-flex align-items-center"
    data-bgimg="assetsuser/img/banner/bannergps.png">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="banner_text">
                    <h2>Amankan Kendaraan Anda</h2>
                    <h1>Dengan GPS Tracker</h1>
                    <p>Dapat Melacak Secara Realtime Dan Mematikan Mesin Jarak Jauh </p>
                    <a href="">Coba Sekarang</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="product_area mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span> <strong>Produk</strong>Terbaru</span></h2>
                </div>
                <div class="product_carousel product_column5 owl-carousel">
                    @foreach($latestProducts as $latestProduct)
                    <div class="single_product">
                        <div class="product_name">
                            <h3><a
                                    href="{{ route('toko.detail', $latestProduct->id) }}">{{ $latestProduct->nama_barang }}</a>
                            </h3>
                            <p class="manufacture_product"><a href="#">{{ $latestProduct->kategori_barang }}</a></p>
                        </div>
                        <div class="product_thumb">
                            <a class="primary_img" href="{{ route('toko.detail', $latestProduct->id) }}"><img
                                    src="{{ asset('storage/gambar_produk/' . $latestProduct->gambar_barang) }}"
                                    alt=""></a>
                        </div>
                        <div class="product_content">
                            <div class="product_footer d-flex align-items-center">
                                <div class="price_box">
                                    <span class="regular_price">Rp.
                                        {{ number_format($latestProduct->harga_barang, 0, ',', '.') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>
</section>

<div class="brand_area mb-42">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="brand_container owl-carousel">
                    <div class="single_brand">
                        <a href="#"><img src="{{asset('assetsuser/img/brand/momentum.jpg')}}"
                                style="width:164px; height:38px;" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="{{asset('assetsuser/img/brand/venom-logo.webp')}}"
                                style="width:164px; height:38px;" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="{{asset('assetsuser/img/brand/kenwood.png')}}"
                                style="width:164px; height:38px;" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="{{asset('assetsuser/img/brand/sansui.png')}}"
                                style="width:164px; height:38px;" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="{{asset('assetsuser/img/brand/skeleton.jpg')}}"
                                style="width:164px; height:38px;" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="blog_section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span> <strong>Blog</strong>Terbaru</span></h2>
                </div>
                <div class="blog_carousel blog_column4 owl-carousel">
                    @foreach($latestBlogs as $blog)
                    <div class="single_blog">
                        <div class="blog_thumb">
                            <a href="{{ route('blogdetail', ['id' => $blog->id]) }}"> <img
                                    src="{{ asset('storage/gambar_blog/'.$blog->gambar_blog) }}"
                                    alt="{{ $blog->judul_blog }}"></a>
                        </div>
                        <div class="blog_content">
                            <div class="date_post">
                                <span>{{ $blog->created_at->format('d M Y') }}</span>
                            </div>
                            <h3><a href="{{ route('blogdetail', ['id' => $blog->id]) }}">{{ $blog->judul_blog }}</a>
                            </h3>
                            <div class="blog_desc">
                                <p>{{ \Illuminate\Support\Str::limit($blog->isi_blog, 150) }}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>





@endsection
