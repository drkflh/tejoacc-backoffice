@extends('layouts.main')

@section('title', $blog->judul_blog)

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="">Beranda</a></li>
                        <li>Blog</li>
                        <li>{{ $blog->judul_blog }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blog_details blog_padding mt-23">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="blog_details_wrapper">
                    <div style="text-align: center;">
                        <a href="#">
                            <img src="{{ asset('storage/gambar_blog/'.$blog->gambar_blog) }}"
                                alt="{{ $blog->judul_blog }}"
                                style="max-width: 1000px; height: auto; display: inline-block;">
                        </a>
                    </div>
                    <div class="blog_content">
                        <h3 class="post_title">{{ $blog->judul_blog }}</h3>
                        <div class="post_meta">
                            <span><i class="ion-person"></i> Diposting Oleh </span>
                            <span><a href="#">Admin</a></span>
                            <span>|</span>
                            <span><i class="fa fa-calendar" aria-hidden="true"></i> Pada Tanggal
                                {{ $blog->created_at->format('d F, Y') }} </span>

                        </div>
                        <div class="post_content">
                            {!! $blog->isi_blog !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
