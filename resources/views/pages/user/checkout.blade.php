@extends('layouts.main')

@section('title', 'Checkout')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Beranda</a></li>
                        <li>Checkout</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="Checkout_section mt-32">
    <div class="container">
        <div class="checkout_form">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <form action="{{ route('checkout.store') }}" method="POST">
                        @csrf
                        @foreach($cartItems as $item)
                        <input type="hidden" name="cartItems[{{ $loop->index }}][product_id]"
                            value="{{ $item->product->id }}">
                        <input type="hidden" name="cartItems[{{ $loop->index }}][product_name]"
                            value="{{ $item->product->nama_barang }}">
                        <input type="hidden" name="cartItems[{{ $loop->index }}][price]"
                            value="{{ $item->product->harga_barang }}">
                        @endforeach

                        <h3>Detail Pelanggan</h3>
                        <div class="row">
                            <div class="col-lg-6 mb-20">
                                <label>Nama <span>*</span></label>
                                <input type="text" name="nama" value="{{ $user->name }}" readonly>
                            </div>
                            <div class="col-lg-6 mb-20">
                                <label>Email <span>*</span></label>
                                <input type="email" name="email" value="{{ $user->email }}" readonly>
                            </div>

                            <div class="col-12 mb-20">
                                <label>Nomer Handphone <span>*</span></label>
                                <input type="text" name="nomer_hp" value="62 {{ $user->nomer_hp }}" readonly>
                            </div>

                            <div class="col-12 mb-20">
                                <label for="lokasi">Lokasi Pemasangan <span>*</span></label>
                                <select class="niceselect_option" name="lokasi_pemasangan" id="lokasi" required>
                                    <option value="" disabled selected>Pilih Lokasi Pemasangan</option>
                                    <option value="DiBengkel Kami">Di Bengkel Kami</option>
                                    <option value="DiRumah Anda">DiRumah Anda</option>
                                </select>
                            </div>

                            <div id="notes">
                                <span class="note">Catatan:</span><br>
                                <span class="note">Ketika Dirumah Maka Akan Ada Biaya Transport & Kami Whatsapp
                                    Untuk Share Lokasi Rumah</span>
                            </div>
                        </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <h3>Orderan Anda</h3>
                    <div class="order_table table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th>Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($cartItems as $item)
                                <tr>
                                    <td>{{ $item->product->nama_barang }}</td>
                                    <td>Rp {{ number_format($item->product->harga_barang, 0, ',', '.') }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="2">Barang Kosong</td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                @if($cartItems->isNotEmpty())
                                <tr class="order_total">
                                    <th>Total Harga Barang</th>
                                    <td>Rp {{ number_format($cartItems->sum('product.harga_barang'), 0, ',', '.') }}
                                    </td>
                                </tr>
                                @endif
                            </tfoot>
                        </table>
                    </div>
                    <div class="payment_method">
                        <div class="order_button">
                            <button type="submit">Bayar Sekarang</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            </d iv>
        </div>
    </div>
    @endsection