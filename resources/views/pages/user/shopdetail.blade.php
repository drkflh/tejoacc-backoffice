@extends('layouts.main')

@section('title', $product->nama_barang)

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Beranda</a></li>
                        <li><a href="/toko">Toko</a></li>
                        <li><a href="#">{{ $product->nama_barang}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="product_details mt-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product-details-tab">
                    <div id="img-1" class="zoomWrapper single-zoom">
                        <a href="#">
                            <img id="zoom1" src="{{ asset('storage/gambar_produk/' . $product->gambar_barang) }}"
                                data-zoom-image="{{ asset('storage/gambar_produk/' . $product->gambar_barang) }}"
                                alt="big-1">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product_d_right">

                    <h1>{{ $product->nama_barang}}</h1>
                    <div class="price_box">
                        <span class="current_price">Rp.
                            {{ number_format($product->harga_barang, 0, ',', '.') }}</span>
                    </div>
                    <div class="product_meta">
                        <span>Stok Tersedia: <a href="#">{{ $product->stok_barang}}</a></span>
                    </div>
                    <div class="product_meta">
                        <span>Kategori: <a href="#">{{ $product->kategori_barang}}</a></span>
                    </div>
                    <div class="product_desc">
                        <p>{{ $product->keterangan_barang}}</p>
                    </div>
                    @auth    
                    <div class="product_variant quantity">
                        <form id="addToCartForm" action="{{ route('keranjang.tambah') }}" method="POST">
                            @csrf
                            <input type="hidden" name="product_id" value="{{ $product->id }}">
                            <button class="button" type="button" id="addToCartButton">Tambah ke Keranjang</button>
                        </form>
                    </div>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
<section class="product_area mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span> <strong>Produk</strong>Terkait</span></h2>
                </div>
                <div class="product_carousel product_column5 owl-carousel">
                    @foreach($relatedProducts as $relatedProduct)
                    <div class="single_product">
                        <div class="product_name">
                            <h3><a
                                    href="{{ route('toko.detail', $relatedProduct->id) }}">{{ $relatedProduct->nama_barang }}</a>
                            </h3>
                            <p class="manufacture_product"><a href="#">{{ $relatedProduct->kategori_barang }}</a></p>
                        </div>
                        <div class="product_thumb">
                            <a class="primary_img" href="{{ route('toko.detail', $relatedProduct->id) }}"><img
                                    src="{{ asset('storage/gambar_produk/' . $relatedProduct->gambar_barang) }}"
                                    alt=""></a>
                        </div>
                        <div class="product_content">
                            <div class="product_footer d-flex align-items-center">
                                <div class="price_box">
                                    <span class="regular_price">Rp.
                                        {{ number_format($relatedProduct->harga_barang, 0, ',', '.') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

    </div>
</section>

<script>
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('addToCartButton').addEventListener('click', function() {
        addToCart();
    });
});

function addToCart() {
    var form = document.getElementById('addToCartForm');
    var formData = new FormData(form);

    fetch(form.action, {
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            body: formData
        })
        .then(response => {
            if (response.ok) {
                Swal.fire({
                    icon: 'success',
                    title: 'Sukses!',
                    text: 'Barang berhasil ditambahkan ke keranjang.',
                });
            } else {
                response.text().then(text => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error!',
                        text: 'Terjadi kesalahan: ' + text,
                    });
                });
            }
        })
        .catch(error => {
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: 'Terjadi kesalahan: ' + error.message,
            });
        });
}
</script>

@endsection