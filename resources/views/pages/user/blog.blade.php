@extends('layouts.main')

@section('title', 'Blog')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="">Beranda</a></li>
                        <li>Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blog_page_section mt-23">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="blog_wrapper">
                    @foreach($blogs as $blog)
                    <div class="single_blog">
                        <div class="blog_thumb">
                            <img src="{{ asset('storage/gambar_blog/'.$blog->gambar_blog) }}"
                                alt="{{ $blog->judul_blog }}" style="width: 426px; height: 292px;">
                        </div>
                        <div class="blog_content">
                            <h3><a href="{{ route('blogdetail', ['id' => $blog->id]) }}">{{ $blog->judul_blog }}</a>
                            </h3>
                            <div class="blog_meta">
                                <span class="post_date"><i class="fa-calendar fa"></i>
                                    {{ $blog->created_at->format('F d, Y') }}</span>
                                <span class="author"><i class="fa fa-user-circle"></i> Diposting Oleh :
                                    Admin</span>
                            </div>
                            <div class="blog_desc">
                                <p
                                    style="max-height: 9em; overflow: hidden; text-overflow: ellipsis; line-height: 1.8em;">
                                    {{ $blog->isi_blog }}
                                </p>
                            </div>
                            <div class="readmore_button">
                                <a href="{{ route('blogdetail', ['id' => $blog->id]) }}">Baca Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>


<div class="blog_pagination">
    <div class="container">
        <d class="row">
            <div class="col-12">
                <div class="pagination">
                    <ul>
                        <li class="current">1</li>
                        <li class="next"><a href="#">next</a></li>
                        <li><a href="#">>></a></li>
                    </ul>
                </div>
            </div>
        </d iv>
    </div>
</div>


@endsection
