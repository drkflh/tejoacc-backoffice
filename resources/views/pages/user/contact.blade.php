@extends('layouts.main')

@section('title', "Kontak Kami")

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="index.html">Beranda</a></li>
                        <li>Kontak Kami</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact_map mt-30">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="map-area">
                    <iframe id="googleMap" width="100%" height="400" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d494.569460904343!2d109.09371225204853!3d-7.403576476069983!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e65617f91e5ef1f%3A0x7e49607a8bc1ce84!2sTEJO%20ACC%20Audio%20%26%20GPS%20Tracking!5e0!3m2!1sid!2sid!4v1705993081236!5m2!1sid!2sid"
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="contact_message content">
                    <h3>Kontak Kami</h3>
                    <p>Tejo Acc Merupakan Sebuah Bengkel Yang Melayani Audio Mobil, Box Costum, Power Window, GPS
                        Tracker, Alarm Mobil, Dan Accecories Mobil Lainya</p>
                    <ul>
                        <li><i class="fa fa-fax"></i>Jl. Pancurendang, Pondokgandu, Pancurendang, Kec. Ajibarang,
                            Kabupaten Banyumas, Jawa Tengah 53163</li>
                        <li><i class="fa fa-envelope-o"></i> <a href="#">darikaflah@gmail.com</a></li>
                        <li><i class="fa fa-phone"></i><a href="wa.me/6282136951197">+62 821 3695 1197</a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="contact_message form">
                    <h3>Beritahu Kami Proyek Anda</h3>
                    <form id="contact-form" method="POST" action="https://htmldemo.net/autima/autima/assets/mail.php">
                        <p>
                            <label> Nama *</label>
                            <input name="name" placeholder="Name *" type="text">
                        </p>
                        <p>
                            <label> Email *</label>
                            <input name="email" placeholder="Email *" type="email">
                        </p>
                        <p>
                            <label> Subject </label>
                            <input name="subject" placeholder="Subject *" type="text">
                        </p>
                        <div class="contact_textarea">
                            <label> Pesan</label>
                            <textarea placeholder="Message *" name="message" class="form-control2"></textarea>
                        </div>
                        <button type="submit"> Send</button>
                        <p class="form-messege"></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>



@endsection