@extends('layouts.main')

@section('title', 'Toko')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="">Beranda</a></li>
                        <li>Toko</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="shop_area shop_reverse">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="shop_title">
                    <h1>TOKO</h1>
                </div>
                <div class="shop_toolbar_wrapper">
                    <div class="search-bar">
                        <form class="search-form" action="{{ route('toko') }}" method="GET">
                            <input type="text" class="search-input" name="search" placeholder="Cari Barang..."
                                value="{{ request('search') }}">
                            <button type="submit" class="search-button"><i class="ion-ios-search-strong"></i></button>
                        </form>
                    </div>

                    <div class="page_amount">
                        <p>Menampilkan {{ $products->firstItem() }}–{{ $products->lastItem() }} dari
                            {{ $products->total() }}
                            Hasil</p>
                    </div>
                </div>
                <div class="row shop_wrapper">
                    @forelse($products as $product)
                    <div class="col-lg-4 col-md-4 col-12 ">
                        <div class="single_product">
                            <div class="product_name grid_name">
                                <h3><a
                                        href="{{ route('toko.detail', ['id' => $product->id]) }}">{{ $product->nama_barang }}</a>
                                </h3>
                                <p class="manufacture_product"><a
                                        href="{{ route('toko.detail', ['id' => $product->id]) }}">{{ $product->kategori_barang }}</a>
                                </p>
                            </div>
                            <div class="product_thumb">
                                <a class="primary_img" href="{{ route('toko.detail', ['id' => $product->id]) }}"><img
                                        src="{{ asset('storage/gambar_produk/' . $product->gambar_barang) }}"
                                        alt=""></a>
                            </div>
                            <div class="product_content grid_content">
                                <div class="content_inner">
                                    <div class="product_footer d-flex align-items-center">
                                        <div class="price_box">
                                            <span class="current_price">Rp.
                                                {{ number_format($product->harga_barang, 0, ',', '.') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="col-12">
                        <p>{{ isset($searchTerm) ? 'Barang tidak ditemukan.' : 'Tidak ada barang yang tersedia.' }}</p>
                    </div>
                    @endforelse
                </div>

                <div class="shop_toolbar t_bottom">
                    <div class="pagination">
                        <ul>
                            @if($products->onFirstPage())
                            <li class="current">1</li>
                            @else
                            <li><a href="{{ $products->previousPageUrl() }}">1</a></li>
                            @endif

                            @if($products->lastPage() > 1)
                            @php
                            $startPage = max(2, $products->currentPage() - 2);
                            $endPage = min($products->lastPage(), $products->currentPage() + 2);
                            @endphp

                            @if($startPage > 2)
                            <li><span>...</span></li>
                            @endif

                            @for($i = $startPage; $i <= $endPage; $i++) @if($i==$products->currentPage())
                                <li class="current">{{ $i }}</li>
                                @else
                                <li><a href="{{ $products->url($i) }}">{{ $i }}</a></li>
                                @endif
                                @endfor

                                @if($endPage < $products->lastPage())
                                    <li><span>...</span></li>
                                    @endif
                                    @endif

                                    @if($products->hasMorePages())
                                    <li class="next"><a href="{{ $products->nextPageUrl() }}">next</a></li>
                                    <li><a href="{{ $products->nextPageUrl() }}">>></a></li>
                                    @endif
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection
