@extends('layouts.main')

@section('title', 'Keranjang')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">Beranda</a></li>
                        <li>Keranjang Belanja</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="shopping_cart_area mt-32">
    <div class="container">
        <form action="#">
            <div class="row">
                <div class="col-12">
                    <div class="table_desc">
                        <div class="cart_page table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="product_remove">Hapus</th>
                                        <th class="product_thumb">Gambar</th>
                                        <th class="product_name">Produk</th>
                                        <th class="product_total">Harga</th>
                                    </tr>
                                </thead>
                                @php
                                $totalPrice = 0;
                                @endphp
                                @forelse ($cartItems as $item)
                                <tr>
                                    <td class="product_remove">
                                        <button class="remove-btn btn btn-danger"
                                            data-item-id="{{ $item->id }}">Hapus</button>
                                    </td>
                                    <td class="product_thumb">
                                        <a href="#">
                                            <img src="{{ asset('storage/gambar_produk/' . $item->product->gambar_barang) }}"
                                                alt="" width="100" height="100">
                                        </a>
                                    </td>
                                    <td class="product_name"><a href="#">{{ $item->product->nama_barang }}</a></td>
                                    <td class="product_total">Rp
                                        {{ number_format($item->product->harga_barang, 0, ',', '.') }}</td>
                                    @php
                                    $totalPrice += $item->product->harga_barang;
                                    @endphp
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Keranjang Anda Kosong</td>
                                </tr>
                                @endforelse
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="coupon_area">
                <div class="row">
                    <div class="">
                        <div class="coupon_code right">
                            <h3>Total Keranjang</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">Rp {{ number_format($totalPrice, 0, ',', '.') }}
                                </div>
                                <div class="checkout_btn">
                                    <a href="{{ route('checkout') }}">Checkout Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
document.querySelectorAll('.remove-btn').forEach(function(button) {
    button.addEventListener('click', function() {
        var itemId = this.getAttribute('data-item-id');
        removeItem(itemId);
    });
});

function removeItem(itemId) {
    fetch(`/keranjang/${itemId}`, {
            method: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        })
        .then(function(response) {
            if (response.ok) {
                Swal.fire({
                    icon: 'success',
                    title: 'Sukses!',
                    text: 'Barang dari keranjang berhasil dihapus.',
                });
            } else {
                Swal.fire({
                    icon: 'success',
                    title: 'Sukses!',
                    text: 'Barang dari keranjang berhasil dihapus.',
                }).then(function() {
                    window.location.reload();
                });
            }
        })
        .catch(function(error) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Terjadi kesalahan: ' + error.message,
            });
        });
}
</script>

@endsection