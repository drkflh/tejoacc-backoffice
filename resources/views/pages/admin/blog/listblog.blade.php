@extends('layouts.app')

@section('title', "Blog")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Blog</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Blog</a></div>
                <div class="breadcrumb-item">Data Blog</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Judul Blog</th>
                                            <th>Gambar Blog</th>
                                            <th>Isi Blog</th>
                                            <th>Dibuat Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($blogs->isEmpty())
                                        <tr>
                                            <td colspan="6" class="text-center">Data blog kosong</td>
                                        </tr>
                                        @else
                                        @foreach($blogs as $item)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $item->judul_blog}}</td>
                                            <td>
                                                <img src="{{ asset('storage/gambar_blog/' . $item->gambar_blog) }}"
                                                    alt="{{ $item->judul_blog}}" width="100" height="100">
                                            </td>

                                            <td>{{ \Illuminate\Support\Str::limit($item->isi_blog, 100, '...') }}</td>
                                            <td>{{ $item->created_at}}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit"
                                                    data-id="{{ $item->id }}" data-toggle="modal"
                                                    data-target="#editModal">
                                                    Edit Blog
                                                </button>
                                                <form action="{{ route('blog.destroy', $item->id) }}" method="POST"
                                                    class="form-delete">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-delete"
                                                        onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">
                                                        <i class="fas fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#addModal">
                        Tambah Data
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Tambah Data Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('blog.store') }}" method="POST" id="form-add" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="judul_blog">Judul Blog:</label>
                        <input type="text" class="form-control" id="judul_blog" name="judul_blog" required>
                    </div>
                    <div class="form-group">
                        <label for="gambar_blog">Gambar Blog (format: .jpg, .jpeg, .png):</label>
                        <input type="file" class="form-control-file" name="gambar_blog" accept="image/jpeg, image/png">
                    </div>
                    <div class="form-group">
                        <label for="isi_blog">Isi Blog:</label>
                        <textarea class="form-control" id="isi_blog" name="isi_blog" rows="4" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Tambah Produk</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Data Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('blog.update', $item->id) }}" method="POST" id="form-edit"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="blog_id" id="blog_id">

                    <div class="form-group">
                        <label for="edit_judul_blog">Judul Blog</label>
                        <input type="text" class="form-control" id="edit_judul_blog" name="edit_judul_blog">
                    </div>

                    <div class="form-group">
                        <label for="edit_gambar_blog">Gambar Blog (format: .jpg, .jpeg, .png):</label>
                        <input type="file" class="form-control-file" id="edit_gambar_blog" name="edit_gambar_blog"
                            accept="image/jpeg, image/png">
                    </div>

                    <div class="form-group">
                        <label for="edit_isi_blog">Keterangan Blog:</label>
                        <textarea class="form-control" id="edit_isi_blog" name="edit_isi_blog" rows="4"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Update Produk</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('.btn-add').on('click', function() {
        $('#judul_blog').val('');
        $('#gambar_blog').val('');
        $('#isi_blog').val('');
        $('#form-add').attr('action', '{{ route("blog.store") }}');
        $('#addModal').modal('show');
    });

    $('.btn-edit').on('click', function() {
        var id = $(this).data('id');
        var judul_blog = $(this).closest('tr').find('td:eq(1)').text();
        var isi_blog = $(this).closest('tr').find('td:eq(2)').text();

        $('#blog_id').val(id);
        $('#edit_judul_blog').val(judul_blog);
        $('#edit_isi_blog').val(isi_blog);

        $('#editModal').modal('show');
    });

});
</script>


@endsection
