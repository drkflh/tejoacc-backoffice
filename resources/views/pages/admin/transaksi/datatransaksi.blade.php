@extends('layouts.app')

@section('title', "Data Transaksi")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Transaksi</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Transaksi</a></div>
                <div class="breadcrumb-item">Lihat Data Transaksi</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama Pelanggan</th>
                                            <th>Email</th>
                                            <th>Nomor Handphone</th>
                                            <th>Lokasi Pemasangan</th>
                                            <th>Barang yang Dibeli</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($transactions as $transaction)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $transaction->nama }}</td>
                                            <td>{{ $transaction->email }}</td>
                                            <td>{{ $transaction->nomer_hp }}</td>
                                            <td>{{ $transaction->lokasi_pemasangan }}</td>
                                            <td>
                                                <ul>
                                                    @foreach ($transaction->items as $item)
                                                    <li>{{ $item->product->nama_barang }} - Rp
                                                        {{ number_format($item->price, 0, ',', '.') }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
