@extends('layouts.app')

@section('title', "Pesan Template")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Pesan Template</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Whatsapp Gateway</a></div>
                <div class="breadcrumb-item">Pesan Template</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama Template</th>
                                            <th>Pesan Template</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($templates as $item)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $item->nama_template }}</td>
                                            <td>
                                                <span class="editable-phone" data-id="{{ $item->id }}"
                                                    data-nomor="{{ $item->pesan_template }}">{{ $item->pesan_template }}</span>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit"
                                                    data-id="{{ $item->id }}" data-toggle="modal"
                                                    data-target="#editModal">
                                                    Edit Template
                                                </button>
                                                <form action="{{ route('template.destroy', $item->id) }}" method="POST"
                                                    class="form-delete">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-delete"
                                                        onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">
                                                        <i class="fas fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#addModal">
                        Tambah Data
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Pesan Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('template.update', $item->id) }}" method="POST" class="form-edit">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="nama_template">Nama Template:</label>
                        <input type="text" class="form-control" id="nama_template" name="nama_template" required>
                    </div>
                    <div class="form-group">
                        <label for="pesan_template">Pesan Template:</label>
                        <textarea class="form-control" id="pesan_template" name="pesan_template" rows="4"
                            required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Tambah Data Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('template.store') }}" method="POST" id="form-add">
                    @csrf
                    <div class="form-group">
                        <label for="nama_template">Nama Template:</label>
                        <input type="text" class="form-control" id="nama_template" name="nama_template" required>
                    </div>
                    <div class="form-group">
                        <label for="pesan_template">Pesan Template:</label>
                        <textarea class="form-control" id="pesan_template" name="pesan_template" rows="4"
                            required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Tambah Template</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('.btn-add').on('click', function() {
        $('#nama_template').val('');
        $('#pesan_template').val('');

        $('#form-add').attr('action', '{{ route("template.store") }}');

        // Menampilkan modal tambah
        $('#addModal').modal('show');
    });

    // Menangani event klik tombol "Edit Template"
    $('.btn-edit').on('click', function() {
        var id = $(this).data('id');
        var nama_template = $(this).closest('tr').find('td:eq(1)').text();
        var pesan_template = $(this).closest('tr').find('td:eq(2) span').data('nomor');

        // Mengisi nilai input pada modal edit dengan data yang ada
        $('#nama_template').val(nama_template);
        $('#pesan_template').val(pesan_template);

        // Menentukan action form-edit dengan data id
        var formEditAction = '{{ route("template.update", ":id") }}'.replace(':id', id);
        $('.form-edit').attr('action', formEditAction);

        // Menampilkan modal edit
        $('#editModal').modal('show');
    });

});
</script>


@endsection