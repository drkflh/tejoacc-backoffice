@extends('layouts.app')

@section('title', "Buku Kontak")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Daftar Kontak</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Whatsapp Gateway</a></div>
                <div class="breadcrumb-item">Daftar Kontak</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama Kontak</th>
                                            <th>Nomer Kontak</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($phonebook as $item)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $item->nama_kontak }}</td>
                                            <td>
                                                <span class="editable-phone" data-id="{{ $item->id }}"
                                                    data-nomor="{{ $item->nomer_kontak }}">{{ $item->nomer_kontak }}</span>
                                            </td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit"
                                                    data-id="{{ $item->id }}" data-toggle="modal"
                                                    data-target="#editModal">
                                                    Edit Nomer
                                                </button>
                                                <form action="{{ route('phonebook.destroy', $item->id) }}" method="POST"
                                                    id="form-delete">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-delete"
                                                        onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">
                                                        <i class="fas fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#addModal">
                        Tambah Data
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Nomer Kontak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('phonebook.update', $item->id) }}" method="POST" id="form-edit">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="editedNomer">Nomer Kontak:</label>
                        <input type="text" class="form-control" id="editedNomer" name="editedNomer" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Tambah Data Kontak</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('phonebook.store') }}" method="POST" id="form-add">
                    @csrf
                    <div class="form-group">
                        <label for="newNama">Nama Kontak:</label>
                        <input type="text" class="form-control" id="newNama" name="newNama" required>
                    </div>
                    <div class="form-group">
                        <label for="newNomer">Nomer Kontak:</label>
                        <input type="text" class="form-control" id="newNomer" name="newNomer" required>
                    </div>
                    <button type="submit" class="btn btn-success">Tambah Data</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $(document).ready(function() {
        $('.btn-edit').on('click', function() {
            var id = $(this).data('id');
            var nomor = $(this).closest('tr').find('.editable-phone').data('nomor');

            $('#editedNomer').val(nomor);

            $('#form-edit').attr('action', '/phonebook/' + id);
        });
    });

    $('.btn-add').on('click', function() {
        $('#newNama').val('');
        $('#newNomer').val('');

        $('#form-add').attr('action', '/phonebook');
    });
});
</script>

@endsection