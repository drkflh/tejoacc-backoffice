@extends('layouts.app')

@section('title', "Riwayat Pesan")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Riwayat Pesan WhatsApp</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">WhatsApp Gateway</a></div>
                <div class="breadcrumb-item">Riwayat Pesan WhatsApp</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Target</th>
                                        <th>Isi Pesan</th>
                                        <th>Jadwal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($messageHistories as $history)
                                    <tr>
                                        <td>{{ $history->target }}</td>
                                        <td>{{ $history->message }}</td>
                                        <td>{{ $history->schedule_timestamp ? \Carbon\Carbon::createFromTimestamp($history->schedule_timestamp, 'UTC')->setTimezone('Asia/Jakarta')->format('Y-m-d H:i:s') : '-' }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection