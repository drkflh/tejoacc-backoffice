@extends('layouts.app')

@section('title', "Kirim Pesan")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Kirim Pesan WhatsApp</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">WhatsApp Gateway</a></div>
                <div class="breadcrumb-item">Kirim Pesan WhatsApp</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-body">
                        <div class="card">
                            <form method="POST" action="{{ route('send-whatsapp-message') }}">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Pilih Nomor Telepon Tujuan *</label>
                                        @foreach ($phonebooks as $phonebook)
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" name="selected_phonebooks[]"
                                                value="{{ $phonebook->id }}">
                                            <label class="form-check-label">{{ $phonebook->nama_kontak }} -
                                                {{ $phonebook->nomer_kontak }}</label>
                                        </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group">
                                        <label>Pilih Tipe Pesan *</label>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" name="message_type"
                                                value="text" checked>
                                            <label class="form-check-label">Text</label>
                                        </div>
                                        <div class="form-check">
                                            <input type="radio" class="form-check-input" name="message_type"
                                                value="template">
                                            <label class="form-check-label">Template</label>
                                        </div>
                                    </div>

                                    <div class="form-group" id="textMessageGroup">
                                        <label>Isi Pesan *</label>
                                        <textarea class="form-control" name="text_message"
                                            id="messageTextArea"></textarea>
                                    </div>

                                    <div class="form-group" id="templateMessageGroup" style="display: none;">
                                        <label>Pilih Template Pesan *</label>
                                        <select class="form-control" name="template_message">
                                            @foreach ($templates as $template)
                                            <option value="{{ $template->pesan_template }}">
                                                {{ $template->nama_template }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Pilih Waktu Pengiriman (WIB)</label>
                                        <p>Jika Kosong Dikirim Sekarang</p>
                                        <input type="datetime-local" class="form-control" name="schedule">
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="submit" class="btn btn-primary">Kirim Pesan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<script>
$(document).ready(function() {
    $('input[name="message_type"]').change(function() {
        console.log('Change event triggered. Selected value:', this.value);

        if (this.value === 'text') {
            console.log('Text message type selected');
            $('#textMessageGroup').show();
            $('#templateMessageGroup').hide();
        } else if (this.value === 'template') {
            console.log('Template message type selected');
            $('#textMessageGroup').hide();
            $('#templateMessageGroup').show();
        }
    });

    $('input[name="message_type"]:checked').trigger('change');

    $('button[type="submit"]').click(function() {
        console.log('Send button clicked');
    });
});
</script>



@endsection