@extends('layouts.app')

@section('title', "Produk")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Produk</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Produk</a></div>
                <div class="breadcrumb-item">Data Produk</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama Barang</th>
                                            <th>Stok Barang</th>
                                            <th>Harga Barang</th>
                                            <th>Kategori Barang</th>
                                            <th>Gambar Barang</th>
                                            <th>Keterangan Barang</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $item)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $item->nama_barang }}</td>
                                            <td>{{ $item->stok_barang }}</td>
                                            <td>{{ 'Rp. ' . number_format($item->harga_barang, 0, ',', '.') }}</td>
                                            <td>{{ $item->kategori_barang }}</td>
                                            <td>
                                                <img src="{{ asset('storage/gambar_produk/' . $item->gambar_barang) }}"
                                                    alt="Gambar {{ $item->nama_barang }}" width="100">
                                            </td>
                                            <td>{{ $item->keterangan_barang }}</td>
                                            <td>
                                                <button type="button" class="btn btn-primary btn-edit"
                                                    data-id="{{ $item->id }}" data-toggle="modal"
                                                    data-target="#editModal">
                                                    Edit Template
                                                </button>
                                                <form action="{{ route('produk.destroy', $item->id) }}" method="POST"
                                                    class="form-delete">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-delete"
                                                        onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">
                                                        <i class="fas fa-trash"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#addModal">
                        Tambah Data
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Tambah Data Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('produk.store') }}" method="POST" id="form-add" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang:</label>
                        <input type="text" class="form-control" id="nama_barang" name="nama_barang" required>
                    </div>
                    <div class="form-group">
                        <label for="stok_barang">Stok Barang:</label>
                        <input type="number" class="form-control" id="stok_barang" name="stok_barang" required>
                    </div>
                    <div class="form-group">
                        <label for="harga_barang">Harga Barang:</label>
                        <input type="number" class="form-control" id="harga_barang" name="harga_barang" required>
                    </div>
                    <div class="form-group">
                        <label for="kategori_barang">Kategori Barang:</label>
                        <select class="form-control" id="kategori_barang" name="kategori_barang" required>
                            <option value="Audio">Audio</option>
                            <option value="Security">Security</option>
                            <option value="Lighting">Lighting</option>
                            <option value="Accecories">Accecories</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="gambar_barang">Gambar Barang (format: .jpg, .jpeg, .png):</label>
                        <input type="file" class="form-control-file" name="gambar_barang"
                            accept="image/jpeg, image/png">
                    </div>
                    <div class="form-group">
                        <label for="keterangan_barang">Keterangan Barang:</label>
                        <textarea class="form-control" id="keterangan_barang" name="keterangan_barang" rows="4"
                            required></textarea>
                    </div>
                    <button type="submit" class="btn btn-success">Tambah Produk</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Edit Data Produk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('produk.update', $item->id) }}" method="POST" id="form-edit"
                    enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="product_id" id="product_id">

                    <div class="form-group">
                        <label for="edit_nama_barang">Nama Barang:</label>
                        <input type="text" class="form-control" id="edit_nama_barang" name="edit_nama_barang">
                    </div>

                    <div class="form-group">
                        <label for="edit_stok_barang">Stok Barang:</label>
                        <input type="number" class="form-control" id="edit_stok_barang" name="edit_stok_barang">
                    </div>

                    <div class="form-group">
                        <label for="edit_harga_barang">Harga Barang:</label>
                        <input type="number" class="form-control" id="edit_harga_barang" name="edit_harga_barang">
                    </div>

                    <div class="form-group">
                        <label for="edit_kategori_barang">Kategori Barang:</label>
                        <select class="form-control" id="edit_kategori_barang" name="edit_kategori_barang">
                            <option value="Audio">Audio</option>
                            <option value="Security">Security</option>
                            <option value="Lighting">Lighting</option>
                            <option value="Accecories">Accecories</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="edit_gambar_barang">Gambar Barang (format: .jpg, .jpeg, .png):</label>
                        <input type="file" class="form-control-file" id="edit_gambar_barang" name="edit_gambar_barang"
                            accept="image/jpeg, image/png">
                    </div>

                    <div class="form-group">
                        <label for="edit_keterangan_barang">Keterangan Barang:</label>
                        <textarea class="form-control" id="edit_keterangan_barang" name="edit_keterangan_barang"
                            rows="4"></textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Update Produk</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <button type="button" class="btn btn-success btn-add" data-toggle="modal" data-target="#addModal">
            Tambah Data
        </button>
    </div>
</div>
</div>
<script>
$(document).ready(function() {
    $('.btn-add').on('click', function() {
        $('#nama_barang').val('');
        $('#stok_barang').val('');
        $('#harga_barang').val('');
        $('#kategori_barang').val('');
        $('#gambar_barang').val('');
        $('#keterangan_barang').val('');
        $('#form-add').attr('action', '{{ route("produk.store") }}');
        $('#addModal').modal('show');
    });

    $('.btn-edit').on('click', function() {
        var id = $(this).data('id');
        var nama_barang = $(this).closest('tr').find('td:eq(1)').text();
        var stok_barang = $(this).closest('tr').find('td:eq(2)').text();
        var harga_barang = $(this).closest('tr').find('td:eq(3)').text();
        var kategori_barang = $(this).closest('tr').find('td:eq(4)').text();
        var keterangan_barang = $(this).closest('tr').find('td:eq(6)').text();

        $('#product_id').val(id);
        $('#edit_nama_barang').val(nama_barang);
        $('#edit_stok_barang').val(stok_barang);
        $('#edit_harga_barang').val(harga_barang);
        $('#edit_kategori_barang').val(kategori_barang);
        $('#edit_keterangan_barang').val(keterangan_barang);

        $('#editModal').modal('show');
    });
});
</script>

@endsection