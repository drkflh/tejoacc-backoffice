@extends('layouts.app')

@section('title', "Perangkat")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Device</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Home</a></div>
                <div class="breadcrumb-item active">Data Device</div>
            </div>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <h5>Device Information</h5>
                    <table class="table">
                        <tr>
                            <td>Name:</td>
                            <td>{{ $deviceData['name'] ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <td>Device Status:</td>
                            <td id="deviceStatus">{{ $deviceData['device_status'] ?? 'N/A' }}</td>
                            <td>
                                <form id="connectForm" action="{{ route('device.connect') }}" method="GET">
                                    @csrf
                                    <button type="submit" class="btn btn-primary" id="connectButton">Connect</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>Expired:</td>
                            <td>{{ $deviceData['expired'] ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <td>Messages:</td>
                            <td>{{ $deviceData['messages'] ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <td>Package:</td>
                            <td>{{ $deviceData['package'] ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <td>Quota:</td>
                            <td>{{ $deviceData['quota'] ?? 'N/A' }}</td>
                        </tr>
                        <tr>
                            <td>Status:</td>
                            <td>{{ $deviceData['status'] ? 'Active' : 'Inactive' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
document.addEventListener('DOMContentLoaded', function() {
    var deviceStatus = document.getElementById('deviceStatus').textContent;

    if (deviceStatus.trim().toLowerCase() === 'connect') {
        document.getElementById('connectForm').style.display = 'none';
    }
});
</script>

@endsection
