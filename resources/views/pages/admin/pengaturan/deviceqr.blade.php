@extends('layouts.app')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data Device</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">Home</a></div>
                <div class="breadcrumb-item active">Data Device</div>
            </div>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <h5>Sambungkan Dengan Whatsaap</h5>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">{{ __('Connecting Device') }}</div>

                                    <div class="card-body text-center">
                                        <img src="data:image/png;base64, {{ $qrCode }}" alt="QR Code" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                    setTimeout(function() {
                        window.close();
                    }, 5000);
                    </script>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection