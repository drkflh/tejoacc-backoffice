@extends('layouts.app')

@section('title', "Tambah Data GPS")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data GPS Tracker</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">GPS TRACKER</a></div>
                <div class="breadcrumb-item">Tambah Data GPS Tracker</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form method="POST" action="{{ route('storedatagps') }}">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Nama Costumer *</label>
                                    <input type="text" class="form-control" name="Nama_Costumer" required>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Pemasangan *</label>
                                    <input type="date" class="form-control" name="Tanggal_Pemasangan" required>
                                </div>
                                <div class="form-group">
                                    <label>Email BYU</label>
                                    <input type="text" class="form-control" name="Email_BYU">
                                </div>
                                <div class="form-group">
                                    <label>Password BYU</label>
                                    <input type="text" class="form-control" name="Password_BYU">
                                </div>
                                <div class="form-group">
                                    <label>Username * </label>
                                    <input type="text" class="form-control" name="Username" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="Password">
                                </div>
                                <div class="form-group">
                                    <label>Imei * </label>
                                    <input type="text" class="form-control" name="Imei" required>
                                </div>
                                <div class="form-group">
                                    <label>Nomer HP * </label>
                                    <input type="text" class="form-control" name="Nomer_HP" required>
                                </div>
                                <div class="form-group">
                                    <label>Masa Aktif * </label>
                                    <input type="date" class="form-control" name="Masa_Aktif" required>
                                </div>
                                <div class="form-group">
                                    <label>Server *</label>
                                    <select class="form-control" name="Server" required>
                                        <option value="" disabled selected>-- Click Untuk Memilih --</option>
                                        <option value="Tracksolid">Tracksolid</option>
                                        <option value="Whatsgps">Whatsgps</option>
                                    </select>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsectionp
