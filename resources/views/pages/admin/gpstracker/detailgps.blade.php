@extends('layouts.app')

@section('title', "Detail Data GPS")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Detail Data GPS {{ $gps->nama_costumer }}</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">GPS TRACKER</a></div>
                <div class="breadcrumb-item">Detail Data GPS {{ $gps->nama_costumer }}</div>
            </div>
        </div>

        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <h3>Detail Data:</h3>
                    <p>Nama Costumer: {{ $gps->nama_costumer }}</p>
                    <p>Tanggal Pemasangan: {{ $gps->tanggal_pemasangan }}</p>
                    <p>Email BYU: {{ $gps->email_byu }}</p>
                    <p>Password BYU: {{ $gps->password_byu }}</p>
                    <p>Username: {{ $gps->username }}</p>
                    <p>Password: {{ $gps->password }}</p>
                    <p>IMEI: {{ $gps->imei }}</p>
                    <p>Nomor HP: 0{{ $gps->nomer_hp }}</p>
                    <p>Masa Aktif Paket: {{ $gps->masa_aktif }}</p>
                    <p>Server: {{ $gps->server }}</p>
                </div>
                <div class="card-footer bg-whitesmoke">
                    <div class="btn-group" role="group" aria-label="Edit and Delete Buttons">
                        <a href="{{ route('editgps', $gps->id) }}" class="btn btn-primary">Edit</a>
                        <form method="POST" action="{{ route('deletegps', $gps->id) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Hapus</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>
@endsection
