@extends('layouts.app')

@section('title', "Data GPS")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Data GPS Tracker</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">GPS TRACKER</a></div>
                <div class="breadcrumb-item">Lihat Data GPS Tracker</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th>Nama Device</th>
                                            <th>Username</th>
                                            <th>Masa aktif</th>
                                            <th>Server</th>
                                            <th>Detail</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($gps as $item)
                                        <tr>
                                            <td>
                                                {{ $loop->index + 1 }}
                                            </td>
                                            <td>{{ $item->nama_costumer }}</td>
                                            <td>{{ $item->username }}</td>
                                            <td>{{ $item->masa_aktif }}</td>
                                            <td>{{ $item->server }}</td>
                                            <td>
                                                <a href="{{ route('detailgps', ['id' => $item->id]) }}"
                                                    class="btn btn-secondary">Detail</a>

                                            </td>
                                            <td>
                                                @php
                                                $masaAktif = \Carbon\Carbon::parse($item->masa_aktif);
                                                $today = \Carbon\Carbon::now();
                                                @endphp

                                                @if ($today->gt($masaAktif))
                                                <div class="alert alert-warning alert-narrow" role="alert">
                                                    Dear {{ $item->nama_costumer }}, GPS Anda dengan username :
                                                    {{ $item->username }}
                                                    dan IMEI : {{ $item->imei }} tidak aktif. Segera isi kouta Anda.
                                                </div>
                                                @elseif ($today->eq($masaAktif))
                                                <div class="alert alert-info alert-narrow" role="alert">
                                                    Dear {{ $item->nama_costumer }}, Masa aktif GPS Anda berakhir hari
                                                    ini. Segera isi kouta Anda.
                                                </div>
                                                @else
                                                <div class="alert alert-success alert-narrow" role="alert">
                                                    Masa aktif GPS Anda masih berlangsung. Tersisa
                                                    {{ $today->diffInDays($masaAktif) }} hari.
                                                </div>
                                                @endif
                                            </td>


                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
