@extends('layouts.app')

@section('title', "Edit Data GPS")

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Edit Data GPS Tracker</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="#">GPS TRACKER</a></div>
                <div class="breadcrumb-item">Edit Data GPS Tracker</div>
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <form method="POST" action="{{ route('updategps', $gps->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="card-body">
                                <div class="form-group">
                                    <label>Nama Costumer *</label>
                                    <input type="text" class="form-control" name="nama_costumer"
                                        value="{{ $gps->nama_costumer }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Pemasangan *</label>
                                    <input type="date" class="form-control" name="tanggal_pemasangan"
                                        value="{{ $gps->tanggal_pemasangan }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Email BYU</label>
                                    <input type="text" class="form-control" name="email_byu"
                                        value="{{ $gps->email_byu }}">
                                </div>
                                <div class="form-group">
                                    <label>Password BYU</label>
                                    <input type="text" class="form-control" name="password_byu"
                                        value="{{ $gps->password_byu }}">
                                </div>
                                <div class="form-group">
                                    <label>Username *</label>
                                    <input type="text" class="form-control" name="username" value="{{ $gps->username }}"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="password"
                                        value="{{ $gps->password }}">
                                </div>
                                <div class="form-group">
                                    <label>Imei *</label>
                                    <input type="text" class="form-control" name="imei" value="{{ $gps->imei }}">
                                </div>
                                <div class="form-group">
                                    <label>Nomer HP *</label>
                                    <input type="text" class="form-control" name="nomer_hp"
                                        value="{{ $gps->nomer_hp }}">
                                </div>
                                <div class="form-group">
                                    <label>Masa Aktif *</label>
                                    <input type="date" class="form-control" name="masa_aktif"
                                        value="{{ $gps->masa_aktif }}">
                                </div>
                                <div class="form-group">
                                    <label>Server *</label>
                                    <select class="form-control" name="server">
                                        <option value="Tracksolid" @if($gps->server === 'Tracksolid') selected
                                            @endif>Tracksolid</option>
                                        <option value="Whatsgps" @if($gps->server === 'Whatsgps') selected
                                            @endif>Whatsgps</option>
                                    </select>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection