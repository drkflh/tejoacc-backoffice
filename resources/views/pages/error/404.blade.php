@extends('layouts.main')

@section('title', '404 Not Found')

@section('content')
<div class="error_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="error_form">
                    <h1>404</h1>
                    <h2>Opps! HALAMAN TIDAK DITEMUKAN</h2>
                    <p>Maaf tetapi halaman yang Anda cari tidak ada. <br> telah dihapus, diubah namanya / masih ada
                        tidak tersedia untuk sementara.</p>
                    <a href="/">Kembali Ke Beranda</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
