@extends('layouts.main')

@section('title', 'Login')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">beranda</a></li>
                        <li>Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-32">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="account_form">
                    <h2>login</h2>
                    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                        @csrf
                        <p>
                            <label>Email <span>*</span></label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        </p>
                        <p>
                            <label>Passwords <span>*</span></label>
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="current-password">
                        </p>
                        <div class="login_submit">
                            <a href="/register">Belum Punya Akun? Daftar Sekarang</a>
                            <label for="remember">
                                <input id="remember" type="checkbox">
                                Ingat Saya
                            </label>
                            <button type="submit">login</button>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
