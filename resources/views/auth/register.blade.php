@extends('layouts.main')

@section('title', 'Register')

@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="/">beranda</a></li>
                        <li>Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-32">
    <div class="container">
        <div class="row">
            <div class="">
                <div class="account_form">
                    <h2>login</h2>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <p>
                            <label>Nama <span>*</span></label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                        <p>
                            <label>Email <span>*</span></label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                        <p>
                            <label>Nomer Handphone <span>*</span></label>
                            <input id="nomer_hp" type="number"
                                class="form-control @error('nomer_hp') is-invalid @enderror" name="nomer_hp"
                                value="{{ old('nomer_hp') }}" required autocomplete="nomer_hp">

                            @error('nomer_hp')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                        <p>
                            <label>Passwords <span>*</span></label>
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </p>
                        <p>
                            <label>Konfirmasi Passwords <span>*</span></label>
                            <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password">
                        </p>
                        <div class="login_submit">
                            <a href="/login">Sudah Punya Akun? Login Sekarang</a>
                            <label for="remember">
                                <input id="remember" type="checkbox">
                                Ingat Saya
                            </label>
                            <button type="submit">login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection