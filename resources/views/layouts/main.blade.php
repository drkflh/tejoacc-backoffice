<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>TEJO ACC - @yield('title')</title>
    <meta name="description" content="">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='description' itemprop='description'
        content='Tejo Acc Merupakan Sebuah Bengkel Yang Melayani Audio Mobil, Box Costum, Power Window, GPS Tracker, Alarm Mobil, Dan Accecories Mobil Lainnya'>
    <meta property="og:description"
        content="Tejo Acc Merupakan Sebuah Bengkel Yang Melayani Audio Mobil, Box Costum, Power Window, GPS Tracker, Alarm Mobil, Dan Aksesoris Mobil Lainnya">
    <meta property="og:title" content="Tejo Acc">
    <meta property="og:url" content="http://current.url.com">
    <meta property="og:type" content="article">
    <meta property="og:locale" content="id_ID">
    <meta property="og:locale:alternate" content="pt-br">
    <meta property="og:locale:alternate" content="pt-pt">
    <meta property="og:locale:alternate" content="en-us">
    <meta property="og:site_name" content="Tejo Acc">
    <meta property="og:image" content="{{asset('assetsuser/img/logo/logo.png')}}">
    <meta property="og:image" content="{{asset('assetsuser/img/logo/logo.png')}}">
    <meta property="og:image" content="{{asset('assetsuser/img/logo/logo.png')}}">
    <meta property="og:image" content="{{asset('assetsuser/img/logo/logo.png')}}">
    <meta property="og:image:url" content="{{asset('assetsuser/img/logo/logo.png')}}">
    <meta property="og:image:size" content="300">

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assetsuser/img/logo/logo.png')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/font.awesome.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/slinky.menu.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('assetsuser/css/style.css')}}">
    <script src="{{asset('assetsuser/js/vendor/modernizr-3.7.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>


</head>

<body>

    @include('partials.user.header')
    @yield('content')
    @include('partials.user.footer')
    <script src="{{asset('assetsuser/js/vendor/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/popper.js')}}"></script>
    <script src="{{asset('assetsuser/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/slick.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.countdown.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.ui.js')}}"></script>
    <script src="{{asset('assetsuser/js/jquery.elevatezoom.js')}}"></script>
    <script src="{{asset('assetsuser/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('assetsuser/js/slinky.menu.js')}}"></script>
    <script src="{{asset('assetsuser/js/plugins.js')}}"></script>
    <script src="{{asset('assetsuser/js/main.js')}}"></script>
</body>


</html>