<header class="header_area header_padding">
    <div class="header_bottom bottom_four sticky-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="main_menu header_position">
                        <nav>
                            <ul>
                                <li> <a href="/"><img src="{{asset('assetsuser/img/logo/logo.png')}}"
                                            style="max-width: 120px; width: 70%;" alt=""></a></li>
                                <li><a href="/">Beranda</a>
                                </li>
                                <li class="mega_items"><a href="/toko">Toko</a></li>
                                <li><a href="blog">Blog</a></li>
                                <li><a href="/kontak">Kontak</a></li>
                                @guest
                                <li class="menu-item-has-children">
                                    <a href="/login">Login</a>
                                </li>
                                @endguest
                                @auth
                                <li class="menu-item-has-children">
                                    <a href="/keranjang">Keranjang</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>

                                @endauth
                            </ul>
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>

<div class="off_canvars_overlay"></div>
<div class="Offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <a href="/"><img src="{{asset('assetsuser/img/logo/logo.png')}}"
                            style="max-width: 120px; width: 70%;" alt=""></a>
                    <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                </div>
                <div class="Offcanvas_menu_wrapper">

                    <div class="canvas_close">
                        <a href="#"><i class="ion-android-close"></i></a>
                    </div>


                    <div class="top_right text-end">
                        <ul>
                            <li><a href="/"><img src="{{asset('assetsuser/img/logo/logo.png')}}"
                                        style="max-width: 120px; width: 70%;" alt=""></a></li>
                        </ul>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            <li class="menu-item-has-children">
                                <a href="/">Beranda</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/toko">Toko</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/blog">Blog</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="/kontak">Kontak</a>
                            </li>
                            @guest
                            <li class="menu-item-has-children">
                                <a href="/login">Login</a>
                            </li>
                            @endguest
                            @auth
                            <li class="menu-item-has-children">
                                <a href="/keranjang">Keranjang</a>
                            </li>
                            <li class="menu-item-has-children">
                                <a href="#" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                            @endauth
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>