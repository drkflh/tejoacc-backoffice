<footer class="footer_widgets">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="widgets_container contact_us">
                        <div class="footer_logo">
                            <a href="/"><img src="{{asset('assetsuser/img/logo/logo.png')}}"
                                    style="max-width: 120px; width: 70%;" alt=""></a>
                        </div>
                        <div class="footer_contact">
                            <p>Tejo Acc Merupakan Sebuah Bengkel Yang Melayani Audio Mobil, Box Costum, Power Window,
                                GPS Tracker, Alarm Mobil, Dan Accecories Mobil Lainya</p>
                            <p><span>Alamat</span>Jl. Pancurendang, Pondokgandu, Pancurendang, Kec. Ajibarang, Kabupaten
                                Banyumas, Jawa Tengah 53163</p>
                            <p><span>Butuh Bantuan?</span>Call: <a href="wa.me/6282136951197">+62 821 3695 1197</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widgets_container widget_menu">
                        <h3>Informasi</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="/">Beranda</a></li>
                                <li><a href="/toko">Toko</a></li>
                                <li><a href="/blog">Blog</a></li>
                                <li><a href="/kontak">Kontak</a></li>
                                @guest
                                <li class="menu-item-has-children">
                                    <a href="/login">Login</a>
                                </li>
                                @endguest
                                @auth
                                <li class="menu-item-has-children">
                                    <a href="/keranjang">Keranjang</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="#" class="dropdown-item has-icon text-danger" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>

                                @endauth
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area">
                        <p>Copyright &copy; 2024 <a href="#">TEJO ACC</a> All Right Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
