<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="/home">BackOffice TEJO ACC</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/home">BTA</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="dropdown {{ request()->is('home*') ? 'active' : '' }}">
                <a href="/home" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">GPS Tracker</li>
            <li class="dropdown {{ request()->is('list-gps') ? 'active' : '' }}">
                <a href="/list-gps" class="nav-link"><i class="fas fa-fire"></i><span>Lihat Data GPS</span></a>
            </li>
            <li class="dropdown {{ request()->is('tambahdata/create') ? 'active' : '' }}">
                <a href="/tambahdata/create" class="nav-link"><i class="fas fa-fire"></i><span>Tambah Data
                        GPS</span></a>
            </li>

            <li class="menu-header">Produk</li>
            <li class="dropdown {{ request()->is('produk') ? 'active' : '' }}">
                <a href="/produk" class="nav-link"><i class="fas fa-fire"></i><span>Daftar Produk</span></a>
            </li>

            <li class="menu-header">Blog</li>
            <li class="dropdown {{ request()->is('blogadmin') ? 'active' : '' }}">
                <a href="/blogadmin" class="nav-link"><i class="fas fa-fire"></i><span>Daftar Blog</span></a>
            </li>

            <li class="menu-header">Member</li>
            <li class="dropdown {{ request()->is('member') ? 'active' : '' }}">
                <a href="/member" class="nav-link"><i class="fas fa-fire"></i><span>Daftar Member</span></a>
            </li>
            <li class="dropdown {{ request()->is('transaksi') ? 'active' : '' }}">
                <a href="/transaksi" class="nav-link"><i class="fas fa-fire"></i><span>Daftar Transaksi</span></a>
            </li>

            <li class="menu-header">Whatsapp Gateway</li>
            <li class="dropdown {{ request()->is('phonebook') ? 'active' : '' }}">
                <a href="/phonebook" class="nav-link"><i class="fas fa-fire"></i><span>Daftar Kontak</span></a>
            </li>
            <li class="dropdown {{ request()->is('template') ? 'active' : '' }}">
                <a href="/template" class="nav-link"><i class="fas fa-fire"></i><span>Pesan Template</span></a>
            </li>
            <li class="dropdown {{ request()->is('sendmessage') ? 'active' : '' }}">
                <a href="/sendmessage" class="nav-link"><i class="fas fa-fire"></i><span>Kirim Pesan</span></a>
            </li>
            <li class="dropdown {{ request()->is('messagehistory') ? 'active' : '' }}">
                <a href="/messagehistory" class="nav-link"><i class="fas fa-fire"></i><span>History Pesan</span></a>
            </li>

            <li class="menu-header">Pengaturan</li>
            <li class="dropdown {{ request()->is('device') ? 'active' : '' }}">
                <a href="/device" class="nav-link"><i class="fas fa-fire"></i><span>Device Info</span></a>
            </li>
        </ul>
    </aside>
</div>