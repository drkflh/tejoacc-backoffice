<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\DataGpsController;
use App\Http\Controllers\PhonebookController;
use App\Http\Controllers\DeviceController;
use App\Http\Controllers\TemplateMessageController;
use App\Http\Controllers\SendMessageController;
use App\Http\Controllers\MessageHistoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\DashboardUserController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Artisan;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/storage-link', function() {
    Artisan::call('storage:link');
});

Route::get('/optimize', function() {
    Artisan::call('optimize');
});

Route::get('/kontak', function () {
     return view('pages.user.contact');
});

Route::get('/error/404', function () {
    return view('pages.error.404');
})->name('notfound');


Route::get('/', [App\Http\Controllers\DashboardUserController::class, 'index'])->name('dashboard');
Route::get('/toko', [App\Http\Controllers\TokoController::class, 'index'])->name('toko');
Route::get('/toko/{id}', [App\Http\Controllers\TokoController::class, 'show'])->name('toko.detail');
Route::get('/blog', [App\Http\Controllers\BlogController::class, 'indexuser'])->name('bloguser');
Route::get('/blog/{id}', [App\Http\Controllers\BlogController::class, 'showuser'])->name('blogdetail');

Route::get('/keranjang', [App\Http\Controllers\CartController::class, 'index'])->name('keranjang');
Route::delete('/keranjang/{id}', [App\Http\Controllers\CartController::class, 'destroy'])->name('keranjang.hapus');
Route::post('/keranjang', [App\Http\Controllers\CartController::class, 'store'])->name('keranjang.tambah');

Route::get('/checkout', [App\Http\Controllers\CheckoutController::class, 'index'])->name('checkout');
Route::post('/checkout/store', [App\Http\Controllers\CheckoutController::class, 'store'])->name('checkout.store');

Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login.page');
Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegisterForm'])->name('register.page');
Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');
Route::post('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // MENU GPS
        Route::get('/list-gps', [App\Http\Controllers\DataGpsController::class, 'index'])->name('listgps');
        Route::get('/tambahdata/create', [App\Http\Controllers\DataGpsController::class, 'create'])->name('tambahdatagps');
        Route::post('/tambahdata', [App\Http\Controllers\DataGpsController::class, 'store'])->name('storedatagps');
        Route::get('/detailgps/{id}', [App\Http\Controllers\DataGpsController::class, 'show'])->name('detailgps');
        Route::delete('/deletegps/{id}', [App\Http\Controllers\DataGpsController::class, 'destroy'])->name('deletegps');
        Route::get('/editgps/{id}/diedit', [App\Http\Controllers\DataGpsController::class, 'edit'])->name('editgps');
        Route::put('/editgps/{id}', [App\Http\Controllers\DataGpsController::class, 'update'])->name('updategps');

            // MENU WHATSAPP GATEWAY
            Route::get('/phonebook', [App\Http\Controllers\PhonebookController::class, 'index'])->name('phonebook');
            Route::delete('/phonebook/{id}', [App\Http\Controllers\PhonebookController::class, 'destroy'])->name('phonebook.destroy');
            Route::put('/phonebook/{id}', [App\Http\Controllers\PhonebookController::class, 'update'])->name('phonebook.update');
            Route::post('/phonebook', [App\Http\Controllers\PhonebookController::class, 'store'])->name('phonebook.store');

            Route::get('/template', [App\Http\Controllers\TemplateMessageController::class, 'index'])->name('template');
            Route::delete('/template/{id}', [App\Http\Controllers\TemplateMessageController::class, 'destroy'])->name('template.destroy');
            Route::put('/template/{id}', [App\Http\Controllers\TemplateMessageController::class, 'update'])->name('template.update');
            Route::post('/template', [App\Http\Controllers\TemplateMessageController::class, 'store'])->name('template.store');

            Route::get('/sendmessage', [App\Http\Controllers\SendMessageController::class, 'index'])->name('sendmessage');
            Route::post('/sendmessagepost', [App\Http\Controllers\SendMessageController::class, 'sendWhatsAppMessage'])->name('send-whatsapp-message');

            Route::get('/messagehistory', [App\Http\Controllers\MessageHistoryController::class, 'index'])->name('message-history.index');

                // MENU PRODUK
                Route::get('/produk', [App\Http\Controllers\ProductController::class, 'index'])->name('produk');
                Route::post('/produk', [App\Http\Controllers\ProductController::class, 'store'])->name('produk.store');
                Route::put('/produk/{id}', [App\Http\Controllers\ProductController::class, 'update'])->name('produk.update');
                Route::delete('/produk/{id}', [App\Http\Controllers\ProductController::class, 'destroy'])->name('produk.destroy');

                // MENU MEMBER
                Route::get('/member', [App\Http\Controllers\MemberController::class, 'index'])->name('member');

                // MENU TRANSAKSI
                Route::get('/transaksi', [App\Http\Controllers\TransactionController::class, 'index'])->name('transaksi');

                // MENU BLOG
                Route::get('/blogadmin', [App\Http\Controllers\BlogController::class, 'index'])->name('blog');
                Route::post('/blogadmin', [App\Http\Controllers\BlogController::class, 'store'])->name('blog.store');
                Route::put('/blogadmin/{id}', [App\Http\Controllers\BlogController::class, 'update'])->name('blog.update');
                Route::delete('/blogadmin/{id}', [App\Http\Controllers\BlogController::class, 'destroy'])->name('blog.destroy');

                    // PENGATURAN WEBSITE
                    Route::get('/device', [App\Http\Controllers\DeviceController::class, 'index'])->name('device');
                    Route::get('/device/connect', [App\Http\Controllers\DeviceController::class, 'connect'])->name('device.connect');
});